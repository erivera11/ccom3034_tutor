#ifndef SWEET_H
#define SWEET_H

#include <iostream>
// Ejemplo
using namespace std;
class SweetArray {
private:	
	int *array;
	int size;
	int qseyo;
public:
	SweetArray();
	SweetArray(int n);
	int& operator[](int i);

friend ostream& operator<<(ostream& out, SweetArray S);

};

#endif

